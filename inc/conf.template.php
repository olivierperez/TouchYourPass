<?php
// Database configuration
const DB_CONNECTIONSTRING = 'mysql:host=HOST;dbname=SCHEMA;port=3306';
const DB_USER = 'USER';
const DB_PASSWORD = 'PASSWORD';

const DB_TABLE_PREFIX = 'typ_';

// Name of the table that store migration script already executed
const MIGRATION_TABLE = 'migration';

// Allow visitors to register (see const.php)
const REGISTERING_MODE = MODE_NO_REGISTRATION;

// Global
const APP_NAME = 'Framadate';
const FROM_MAIL = 'admin@example.org';
const REPLYTO_MAIL = 'replyto@example.org';
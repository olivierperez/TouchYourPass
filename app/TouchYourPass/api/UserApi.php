<?php
namespace TouchYourPass\api;

use TouchYourPass\ServiceFactory;

class UserApi extends Api {

    private $userService;

    function __construct() {
        $this->userService = ServiceFactory::userService();
    }

    /**
     * Add a user.
     *
     * @return object The object to return as JSON
     */
    function onPost() {
        $data = json_decode($_POST['data']);

        if (isset($data->mode) && $data->mode == 'activate') {
            $user = $this->userService->activate($data->id, $data->mailContent);
            return array('mode'=> 'activate', 'user' => $user);
        } else {
            if (isset($data->id)) {
                return $this->update($data);
            } else {
                return $this->create($data);
            }
        }
    }

    function onGet() {
        return $this->userService->findAll();
    }

    function onDelete() {
        return $this->notImplemented();
    }

    private function update($data) {
        return $this->userService->update($data);
    }

    private function create($data) {
        if (empty($data->name) ||
            empty($data->mail) ||
            empty($data->passphrase) ||
            $data->passphrase == 'd96b7e56fa181a09ef450fbe5db9ef957a93ee7380df25fed8670e97ce6ce632f5da8c567fb59f8afd13e50492626270498bd593cf1f52d63b40744cdb11e58f'
        ) {
            return $this->badRequest();
        }

        $registeringResult = $this->userService->create($data->name, $data->mail, $data->passphrase);

        if ($registeringResult->getLeft()) {
            return array('msg' => __('Register', $registeringResult->getRight()));
        } else {
            return $this->conflict(__('Register', $registeringResult->getRight()));
        }
    }
}

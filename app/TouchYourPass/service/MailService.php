<?php
namespace TouchYourPass\service;

class MailService {

    function __construct() {
    }

    public function isValidEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Send a mail.
     *
     * @param string $to The reciptient mail address
     * @param string $subject The mail subject
     * @param string $body The message to send
     * @return bool {@code true} when the mail is sent
     */
    function send($to, $subject, $body) {
        mb_internal_encoding('UTF-8');

        // Build headers

        $subject = mb_encode_mimeheader(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'), 'UTF-8', 'B', "\n", 9);

        $encoded_app = mb_encode_mimeheader(APP_NAME, 'UTF-8', 'B', "\n", 6);
        $size_encoded_app = (6 + strlen($encoded_app)) % 75;
        $size_admin_email = strlen(FROM_MAIL);

        if (($size_encoded_app + $size_admin_email + 9) > 74) {
            $folding = "\n";
        } else {
            $folding = '';
        };

        $from = sprintf("From: %s%s <%s>\n", $encoded_app, $folding, FROM_MAIL);

        $headers = $from;
        $headers .= 'Reply-To: ' . REPLYTO_MAIL . "\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\n";
        $headers .= "Content-Transfer-Encoding: 8bit\n";
        $headers .= "Auto-Submitted:auto-generated\n";
        $headers .= 'Return-Path: <>';

        // Build body

        $body = $body . '<br/><br/>' . __('Mail', 'Thanks for your trust.') . '<br/>' . APP_NAME . '<hr/>' . __('Mail', 'FOOTER');

        // Send mail

        return @mail($to, $subject, $body, $headers, '');
    }

}

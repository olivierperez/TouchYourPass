<?php
namespace TouchYourPass\service;

use TouchYourPass\Pair;
use TouchYourPass\repository\UserRepository;
use TouchYourPass\ServiceFactory;

class UserService {

    private $userRepository;

    function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    public function authenticate($name, $passphrase) {
        $user = $this->userRepository->findByName($name);

        if ($user !== false && $user->active == 1 && $this->verifyPassphrase($passphrase, $user->passphrase)) {
            $_SESSION['user'] = $user;
            return $user;
        }
        $_SESSION['user'] = null;

        return false;
    }

    /**
     * Use bcrypt with cost of 20.
     *
     * @param string $passphrase The passphrase to hash
     * @return bool|string Then hashed passphrase, or false
     */
    private function hash($passphrase) {
        return password_hash($passphrase, PASSWORD_BCRYPT, array('cost'=>10));
    }

    /**
     * Check if the passphrase is matching to the stored hash.
     *
     * @param string $passphrase The passphrase given by the user
     * @param string $hash The hash stored in database
     * @return bool true is the passphrase matches the hash
     */
    public function verifyPassphrase($passphrase, $hash) {
        return password_verify($passphrase, $hash);
    }

    public function authorizedToSeeUser($userId) {
        if (!empty($_SESSION['user'])) {
            return $userId === $_SESSION['user']->id;
        }

        return false;
    }

    public function findById($userId) {
        return $this->userRepository->findById($userId);
    }

    public function create($name, $mail, $passphrase) {
        if (REGISTERING_MODE == MODE_NO_REGISTRATION) {
            return new Pair(false, 'CANT_REGISTER_IN_THIS_MODE');
        }

        $user = $this->userRepository->findByName($name);

        if ($user) {
            return new Pair(false, 'USER_ALREADY_EXISTS');
        } else {
            if (REGISTERING_MODE == MODE_AUTO_VALIDATION) {
                $this->userRepository->save($name, $mail, $this->hash($passphrase), true);
                ServiceFactory::mailService()->send($mail, __('Mail', 'REGISTERING_SUBJECT'), __('Mail', 'AUTO_REGISTERING_ESSAGE'));
                return new Pair(true, 'GO_TO_CHECK_YOUR_MAIL');
            } else {
                $this->userRepository->save($name, $mail, $this->hash($passphrase), false);
                return new Pair(true, 'WAIT_FOR_VALIDATION');
            }
        }
    }

    public function findAll() {
        return $this->userRepository->findAll();
    }

    public function update($user) {
        if ($this->userRepository->update($user)) {
            return $user;
        } else {
            return false;
        }
    }

    /**
     * @param $userId
     * @param $mailContent
     * @return object User updated
     */
    public function activate($userId, $mailContent) {
        $user = $this->userRepository->findById($userId);
        $user->active = true;
        $this->userRepository->update($user);

        ServiceFactory::mailService()->send($user->mail, __('Mail', 'REGISTERING_SUBJECT'), $mailContent);

        return $user;
    }

}

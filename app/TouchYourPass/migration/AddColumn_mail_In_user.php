<?php
namespace TouchYourPass\migration;

class AddColumn_mail_In_user extends Migration {

    function __construct() {
    }

    function description() {
        return 'Add the "mail" column to the "user" table';
    }

    function preCondition(\PDO $pdo) {
        $stmt = $pdo->query('SHOW TABLES');
        $tables = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        // Check if tables of v0.9 are presents
        $diff = array_diff([$this->prefix('user'), $this->prefix('entry'), $this->prefix('group')], $tables);
        return count($diff) === 0;
    }

    function execute(\PDO $pdo) {
        $this->addColumn($pdo);
    }

    private function addColumn(\PDO $pdo) {
        $pdo->exec('
        ALTER TABLE `' . $this->prefix('user') . '`
        ADD `mail` CHAR(255) NOT NULL
        AFTER `name`
        ');
    }
}

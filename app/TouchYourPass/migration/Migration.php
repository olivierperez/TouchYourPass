<?php
namespace TouchYourPass\Migration;

abstract class Migration {

    /**
     * This method should describe in english what is the purpose of the migration class.
     *
     * @return string The description of the migration class
     */
    abstract function description();

    /**
     * This method could check if the execute method should be called.
     * It is called before the execute method.
     *
     * @param \PDO $pdo The connection to database
     * @return bool true if the Migration should be executed
     */
    abstract function preCondition(\PDO $pdo);

    /**
     * This methode is called only one time in the migration page.
     *
     * @param \PDO $pdo The connection to database
     * @return bool true if the execution succeeded
     */
    abstract function execute(\PDO $pdo);

    protected function prefix($tableName) {
        return DB_TABLE_PREFIX . $tableName;
    }

}

<?php
namespace TouchYourPass;

class Pair {

    private $left;
    private $right;

    public function __construct($left, $right) {
        $this->left = $left;
        $this->right = $right;
    }

    public function getLeft() {
        return $this->left;
    }

    public function getRight() {
        return $this->right;
    }


}

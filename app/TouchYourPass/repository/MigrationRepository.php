<?php
namespace TouchYourPass\repository;

use TouchYourPass\Migration\Migration;

class MigrationRepository extends Repository {

    function __construct($pdo) {
        parent::__construct($pdo);
    }

    /**
     * Find all tables in database.
     *
     * @return array The array of table names
     */
    private function allTables() {
        $result = $this->query('SHOW TABLES');
        $schemas = $result->fetchAll(\PDO::FETCH_COLUMN);

        return $schemas;
    }

    public function prepareSelection() {
        return $this->prepare('SELECT id FROM ' . $this->prefix(MIGRATION_TABLE) . ' WHERE name=?');
    }

    public function prepareInsertion() {
        return $this->prepare('INSERT INTO ' . $this->prefix(MIGRATION_TABLE) . ' (name) VALUES (?)');
    }

    public function preCondition(Migration $migration) {
        return $migration->preCondition($this->pdo);
    }

    public function execute(Migration $migration) {
        $migration->execute($this->pdo);
    }

    public function createMigrationTableIfNeeded() {
        $tables = $this->allTables();

        if (!in_array($this->prefix(MIGRATION_TABLE), $tables)) {
            $this->exec('
CREATE TABLE IF NOT EXISTS `' . $this->prefix(MIGRATION_TABLE) . '` (
  `id`   INT(11)  UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` TEXT              NOT NULL,
  `execute_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = utf8;');
        }
    }

}

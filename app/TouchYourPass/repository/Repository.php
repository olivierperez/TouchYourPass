<?php
namespace TouchYourPass\repository;

abstract class Repository {

    protected $pdo;

    function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }

    protected function prepare($sql) {
        return $this->pdo->prepare($sql);
    }

    protected function query($sql) {
        return $this->pdo->query($sql);
    }

    protected function exec($sql) {
        $this->pdo->exec($sql);
    }

    protected function lastInsertId() {
        return $this->pdo->lastInsertId();
    }

    protected function prefix($table) {
        return DB_TABLE_PREFIX . $table;
    }

}

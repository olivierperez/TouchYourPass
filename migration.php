<?php

use TouchYourPass\migration\AddColumn_mail_In_user;
use TouchYourPass\Migration\Migration;
use TouchYourPass\ServiceFactory;

include_once __DIR__ . '/inc/init.php';

set_time_limit(300);

// List a Migration sub classes to execute
$migrations = [
    new AddColumn_mail_In_user(),
];
// ---------------------------------------

// Migration repository
$migrationRepository = ServiceFactory::migrationRepository();

// Check if MIGRATION_TABLE already exists

$migrationRepository->createMigrationTableIfNeeded();

$selectStmt = $migrationRepository->prepareSelection();
$insertStmt = $migrationRepository->prepareInsertion();
$countSucceeded = 0;
$countFailed = 0;
$countSkipped = 0;

// Loop on every Migration sub classes
$success = [];
$fail = [];
foreach ($migrations as $migration) {
    $className = get_class($migration);

    // Check if $className is a Migration sub class
    if (!$migration instanceof Migration) {
        $smarty->assign('error', 'The class ' . $className . ' is not a sub class of TouchYourPass\\Migration\\Migration.');
        $smarty->display('error.tpl');
        exit;
    }

    // Check if the Migration is already executed
    $selectStmt->execute([$className]);
    $executed = $selectStmt->rowCount();
    $selectStmt->closeCursor();

    if (!$executed && $migrationRepository->preCondition($migration)) {
        $migrationRepository->execute($migration);
        if ($insertStmt->execute([$className])) {
            $countSucceeded++;
            $success[] = $migration->description();
        } else {
            $countFailed++;
            $fail[] = $migration->description();
        }
    } else {
        $countSkipped++;
    }

}

$countTotal = $countSucceeded + $countFailed + $countSkipped;

$smarty->assign('success', $success);
$smarty->assign('fail', $fail);

$smarty->assign('countSucceeded', $countSucceeded);
$smarty->assign('countFailed', $countFailed);
$smarty->assign('countSkipped', $countSkipped);
$smarty->assign('countTotal', $countTotal);
$smarty->assign('time', round((microtime(true)-$_SERVER['REQUEST_TIME_FLOAT']), 4));

$smarty->assign('title', __('Admin', 'Migration'));

$smarty->assign('hideNavBar', true);

$smarty->display('migration.tpl');

{extends file='page.tpl'}

{block name=head}
    <script type="text/javascript">
        define('users', ['ajaxify'], function (ajaxify) {

            var onSuccess = function (response) {
                console.log('success', response);

                if (response.mode == 'activate') {
                    $('#activateModal').hide();
                    // Update the corresponding tr in the table
                    $('#users').find('tr').each(function (index, tr) {
                        if($(tr).find('.id').text() == response.user.id) {
                            applyUserOnTr(response.user, $(tr));
                        }
                    });
                } else {
                    // Empty previous table
                    $('#users').find('tr').each(function (index, tr) {
                        tr = $(tr);
                        if (index != 0 && tr.attr('id') != 'user-model') {
                            tr.remove();
                        }
                    });
                    for (var x in response) {
                        displayUser(response[x]);
                    }
                }
            };

            var onFail = function (status, response) {
                console.log('fail', response);
            };

            var showActivationModal = function (id, name) {
                $('#activateModal')
                        .find('h4 span').text(name).end()
                        .find('input[name=id]').val(id).end()
                        .modal();
            };

            var displayUser = function (user) {
                // Clone the model
                var trModel = $('#user-model');
                var tr = trModel.clone().attr('id', '');
                trModel.before(tr);

                // Apply attributes
                applyUserOnTr(user, tr);

                // Apply attributes on links
                var activateBtn = tr.find('.activated').find('.no');
                var deactivateBtn = tr.find('.activated').find('.yes');

                // Hanlde clicks on buttons
                var refreshActivateButtons = function () {
                    if (user.active == 1) {
                        activateBtn.hide();
                        deactivateBtn.show();
                    } else {
                        deactivateBtn.hide();
                        activateBtn.show();
                    }
                };

                var toggleActiveUser = function (ev, mailContent) {
                    user.active ^= 1;
//                    console.log('onClick', ev, ev.target, user);

                    var target = $(ev.target);

                    var method = target.attr('data-method');
                    var url = target.attr('href');
                    var formData = new FormData();
                    formData.append('data', JSON.stringify(user));
                    mailContent && formData.append('mailContent', mailContent);

                    // Send data
                    var xhr = new XMLHttpRequest();
                    xhr.open(method, url, true);
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState === 4) {
                            if (xhr.status >= 200 && xhr.status < 300) {
                                user = JSON.parse(xhr.response);
                                refreshActivateButtons();
                            } else {
                                console.log('fail', xhr.status, JSON.parse(xhr.response))
                            }
                        }
                    };
                    xhr.send(formData);

                    return false;
                };

                activateBtn.on('click', function (ev) {
                    ev.preventDefault();
                    showActivationModal(user.id, user.name);
                });
                deactivateBtn.on('click', toggleActiveUser);

                tr.fadeIn('fast');
            };

            function applyUserOnTr(user, tr) {
                var activateBtn = tr.find('.activated').find('.no');
                var deactivateBtn = tr.find('.activated').find('.yes');

                tr.find('.id').html(user.id);
                tr.find('.name').html(user.name);
                tr.find('.mail').html(user.mail);

                if (user.active == 1) {
                    activateBtn.hide();
                    deactivateBtn.show();
                } else {
                    deactivateBtn.hide();
                    activateBtn.show();
                }
            }

            return {
                onSuccess: onSuccess,
                onFail: onFail
            };
        });
    </script>
{/block}

{block name=main}
    <h1>{__('Title', 'Users')}</h1>
    <form action="{$SERVER_URL}/api.php?s=user" method="GET" data-module="users" data-module-auto="auto">
        <input type="hidden" name="mode" value="load"/>
        <input type="submit" value="{__('Generic', 'Load')}" class="btn btn-default"/>
    </form>
    <div class="panel panel-default">
        <table id="users" class="table table-striped table-bordered">
            <tr>
                <th width="50px">{__('Generic', 'id')}</th>
                <th width="50px">{__('Generic', 'Active')}</th>
                <th>{__('Generic', 'Name')}</th>
                <th>{__('Generic', 'Mail')}</th>
            </tr>
            <tr id="user-model" style="display:none">
                <td class="id">id</td>
                <td class="activated">
                    <a href="{$SERVER_URL}/api.php?s=user" data-method="POST" class="glyphicon glyphicon-ok yes" aria-hidden="true"></a>
                    <a href="#" class="glyphicon glyphicon-ban-circle no" aria-hidden="true"></a>
                </td>
                <td class="name">name</td>
                <td class="mail">mail</td>
            </tr>
        </table>
    </div>

    <div id="activateModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{$SERVER_URL}/api.php?s=user" method="POST" data-module="users">
                    <input type="hidden" name="mode" value="activate" />
                    <input type="hidden" name="id" value="" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{__('Users', 'Activation: ')}<span></span></h4>
                    </div>
                    <div class="modal-body">
                        <p>{__('Users', 'ACTIVATION_ALERT')}</p>
                        <textarea name="mailContent" class="col-xs-12" rows="10" style="resize:vertical;min-height:100px">{__('Users', 'VALIDATION_MAIL_CONTENT')}</textarea>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{__('Generic', 'Cancel')}</button>
                        <button type="submit" class="btn btn-primary">{__('Generic', 'Apply')}</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
{/block}
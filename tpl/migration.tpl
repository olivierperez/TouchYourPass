{extends file='page.tpl'}

{block name=head}
{/block}

{block name=main}
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <h2>{__('Migration', 'Summary')}</h2>
            {__('Migration', 'Succeeded:')} <span class="label label-warning">{$countSucceeded|html} / {$countTotal|html}</span>
            <br/>
            {__('Migration', 'Failed:')} <span class="label label-danger">{$countFailed|html} / {$countTotal|html}</span>
            <br/>
            {__('Migration', 'Skipped:')} <span class="label label-info">{$countSkipped|html} / {$countTotal|html}</span>
        </div>
        <div class="col-xs-12 col-md-4">
            <h2>{__('Migration', 'Success')}</h2>
            <ul>
                {foreach $success as $s}
                    <li>{$s|html}</li>
                    {foreachelse}
                    <li>{__('Migration', 'Nothing')}</li>
                {/foreach}
            </ul>
        </div>

        <div class="col-xs-12 col-md-4">
            <h2>{__('Migration', 'Fail')}</h2>
            <ul>
                {foreach $fail as $f}
                    <li>{$f|html}</li>
                    {foreachelse}
                    <li>{__('Migration', 'Nothing')}</li>
                {/foreach}
            </ul>
        </div>


        <div class="col-xs-6 col-xs-offset-3 well well-sm text-center">
            {__('Generic', 'Page generated in')} {$time} {__('Generic', 'seconds')}
        </div>
    </div>
{/block}